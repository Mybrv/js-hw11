//1. Додати новий абзац по кліку на кнопку:
//По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і
//додайте його до розділу <section id="content">

const btn = document.querySelector("#btn-click");

const content = document.querySelector("#content")

btn.addEventListener("click", event => {
    const paragraph = document.createElement("p")
    content.prepend(paragraph)
    paragraph.innerText = "New Paragraph"
}, {once: true})

//2. Додати новий елемент форми із атрибутами:
//Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type,
//placeholder, і name. та додайте його під кнопкою.

const btnInput = document.createElement("button")
const footer = document.querySelector("footer")
btnInput.setAttribute("id", "btn-input-create")
btnInput.innerText = "Create Input"
content.insertAdjacentElement("beforeend", btnInput)
btnInput.addEventListener("click", event => {
const input = document.createElement("input");
input.setAttribute("type", "e-mail");
input.setAttribute("placeholder", "Enter e-mail");
input.style.display = "block"
input.style.marginLeft = "auto"
input.style.marginRight = "auto"
input.style.marginTop = "5px"
content.append(input)
}, {once: true})